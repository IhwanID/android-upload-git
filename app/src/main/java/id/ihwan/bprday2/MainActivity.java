package id.ihwan.bprday2;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    //Deklarasi
    private Button pindah, pindahData, cekWeb;
    private EditText nama,urlweb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //inisialisasi
        pindah = findViewById(R.id.btn_pindah_activity);
        pindahData = findViewById(R.id.btn_pindah_activity_dengan_data);
        nama = findViewById(R.id.nama);
        urlweb = findViewById(R.id.url_web);
        cekWeb = findViewById(R.id.cekweb);


        //set onClik
        pindah.setOnClickListener(this);
        pindahData.setOnClickListener(this);
        cekWeb.setOnClickListener(this);

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_pindah_activity:
                Intent pindahIntent = new Intent(MainActivity.this, SecondActivity.class);
                startActivity(pindahIntent);
                break;
            case R.id.btn_pindah_activity_dengan_data:
                Intent pindahDataIntent = new Intent(MainActivity.this, ResultActivity.class);
                pindahDataIntent.putExtra("NAMA", nama.getText().toString());
                startActivity(pindahDataIntent);
                break;
            case R.id.cekweb:
                Intent webviewIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(urlweb.getText().toString()));
                startActivity(webviewIntent);
                break;

        }
    }
}
