package id.ihwan.bprday2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class ResultActivity extends AppCompatActivity {

    private TextView greeting;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        greeting = findViewById(R.id.greeting);

        String name = getIntent().getStringExtra("NAMA");
        greeting.setText("Hi " + name + " apa kabar nih, semoga sehat selalu ya :)");

    }
}
